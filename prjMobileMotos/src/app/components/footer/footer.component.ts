import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {

  actived1 = false;

  actived2 = false;

  actived3 = false;

  actived4 = false;

  actived5 = false;


  constructor() { }

  ngOnInit(): void {
  }

  drawer1(){
    this.actived1 = !this.actived1;
  }

  drawer2(){
    this.actived2 = !this.actived2;
  }

  drawer3(){
    this.actived3 = !this.actived3;
  }

  drawer4(){
    this.actived4 = !this.actived4;
  }

  drawer5(){
    this.actived5 = !this.actived5;
  }
}
