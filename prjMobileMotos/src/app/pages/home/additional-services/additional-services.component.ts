import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-additional-services',
  templateUrl: './additional-services.component.html',
  styleUrls: ['./additional-services.component.scss'],
})
export class AdditionalServicesComponent implements OnInit {

  additionalServices = [

    {
      index: 0,
      cover: './../../../../assets/images/additional-services__Servicos.png',
      title: 'serviços',
      resume: 'Escolha o plano de manutenção que cabe no seu orçamento e agende seu serviço on-line.'
    },
    {
      index: 1,
      cover: './../../../../assets/images/additional-services__pecas.jpg',
      title: 'peças',
      resume: 'Escolha peças genuínas e mantenha o DNA da sua motocicleta 100% original.'
    }

  ];

  constructor() { }

  ngOnInit() {}

}
