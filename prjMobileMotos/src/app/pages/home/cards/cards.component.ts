import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import KeenSlider from 'keen-slider';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: [
    './../../../../../node_modules/keen-slider/keen-slider.min.css',
    './cards.component.scss'],
})
export class CardsComponent implements OnInit {

  cardsStreet = [

    {
      index: 0,
      cover: './../../../../../assets/images/cards__cards-one__CB1000R-Black.png',
      motorcycle: 'CB1000R',
      brand: 'honda'
    },
    {
      index: 1,
      cover: './../../../../../assets/images/cards__cards-one__CB500F.png',
      motorcycle: 'CB500F',
      brand: 'honda'
    },
    {
      index: 2,
      cover: './../../../../../assets/images/cards__cards-one__Fazer-FZ25.png',
      motorcycle: 'Fazer FZ25',
      brand: 'yamaha'
    },
    {
      index: 3,
      cover: './../../../../../assets/images/cards__cards-one__CG-160-Cargo.png',
      motorcycle: 'CG 160',
      brand: 'honda'
    },
    {
      index: 4,
      cover: './../../../../../assets/images/cards__cards-one__CB-300.png',
      motorcycle: 'CB 300',
      brand: 'honda'
    },
    {
      index: 5,
      cover: './../../../../../assets/images/cards__cards-one__G-310-R.png',
      motorcycle: 'g 310 r',
      brand: 'bmw'
    }
  ];

  cardsScooter = [

    {
      index: 0,
      cover: './../../../../../assets/images/cards__cards-two__XMAX-ABS.png',
      motorcycle: 'XMAX ABS',
      brand: 'yamaha'
    },
    {
      index: 1,
      cover: './../../../../../assets/images/cards__cards-two__ADV.png',
      motorcycle: 'Honda ADV',
      brand: 'honda'
    },
    {
      index: 2,
      cover: './../../../../../assets/images/cards__cards-two__FLUO-ABS.png',
      motorcycle: 'FLUO ABS',
      brand: 'yamaha'
    },
    {
      index: 3,
      cover: './../../../../../assets/images/cards__cards-two__Biz-125.png',
      motorcycle: 'biz 125',
      brand: 'honda'
    },
    {
      index: 4,
      cover: './../../../../../assets/images/cards__cards-two__NMax-160.png',
      motorcycle: 'NMax 160',
      brand: 'Yamaha'
    },
    {
      index: 5,
      cover: './../../../../../assets/images/cards__cards-two__X-ADV.png',
      motorcycle: 'X ADV',
      brand: 'honda'
    }
  ];

  @ViewChild("sliderRefStreet") sliderRefStreet: ElementRef<HTMLElement>

  @ViewChild("sliderRefScooter") sliderRefScooter: ElementRef<HTMLElement>

  slider: null;

  constructor() { }

  ngAfterViewInit() {
    this.slider = new KeenSlider(this.sliderRefStreet.nativeElement, {
      loop: true,
      slides: {
        perView: 2,
        spacing: 15,
      },
    });
    this.slider = new KeenSlider(this.sliderRefScooter.nativeElement, {
      loop: true,
      slides: {
        perView: 2,
        spacing: 15,
      },
    });
  };

  ngOnInit() {}

}
