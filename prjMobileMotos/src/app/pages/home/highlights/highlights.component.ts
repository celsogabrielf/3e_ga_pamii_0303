import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-highlights',
  templateUrl: './highlights.component.html',
  styleUrls: ['./highlights.component.scss'],
})
export class HighlightsComponent implements OnInit {

  highlights = [

    {
      index: 0,
      cover: './../../../../../assets/images/highlights__section-two__VERSYS-X-300.jpg',
      motorcycle: 'VERSYS-X 300 ',
      category: 'Trail'
    },
    {
      index: 1,
      cover: './../../../../../assets/images/highlights__section-two__K-1600-GTL.jpg',
      motorcycle: 'K 1600 GTL',
      category: 'Touring'
    }

  ]

  constructor() { }

  ngOnInit() {}

}
