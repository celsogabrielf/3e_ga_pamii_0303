import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import { HighlightsComponent } from './highlights/highlights.component';
import { CardsComponent } from './cards/cards.component';
import { AdditionalServicesComponent } from './additional-services/additional-services.component';
import { FooterComponent } from '../../components/footer/footer.component';
import { SliderComponent } from './slider/slider.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage, HighlightsComponent, CardsComponent, AdditionalServicesComponent, FooterComponent, SliderComponent]
})
export class HomePageModule {}
