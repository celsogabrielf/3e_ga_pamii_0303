import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
})
export class SliderComponent implements OnInit {

  slides = [

    {
      index: 0,
      cover: './../../../../assets/images/slider__CBR-1000RR-R.jpg',
      motorcycle: 'CBR 1000RR-R ',
      category: 'Sport',
      active: 'active'
    },
    {
      index: 1,
      cover: './../../../../assets/images/slider__CMX1100.png',
      motorcycle: 'CMX1100',
      category: 'custom'
    },
    {
      index: 2,
      cover: './../../../../assets/images/slider__R-1250-GS.jpg',
      motorcycle: 'R 1250 GS',
      category: 'trail'
    }

  ];

  constructor() { }

  ngOnInit() {}

}
